﻿using Dapper;
using Microsoft.Extensions.Options;
using MoxyCreditUnion.DataAccess;
using MoxyCreditUnion.Entities;
using MoxyCreditUnion.Helpers;
using MoxyCreditUnion.Repositories.Interfaces;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Z.Dapper.Plus;

namespace MoxyCreditUnion.Repositories.Implementations
{
    public class MoxieCreditUnionRepository : IMoxieCreditUnionRepository
    {
        private readonly string _connectionString;
        private readonly string _truncateStoredProcedureName;
        private readonly string _bulkInsertTableName;

        public MoxieCreditUnionRepository(IOptions<ConnectionConfig> connectionConfig, IOptions<ConstantsConfig> constantsConfig)
        {
            _connectionString = connectionConfig.Value.MoxyLive;
            _truncateStoredProcedureName = constantsConfig.Value.MoxyCuTruncateStoredProcedureName;
            _bulkInsertTableName = constantsConfig.Value.MoxyCuTableName;
        }

        public bool SaveMoxieCreditUnionData(IEnumerable<MoxyCreditUnionEntity> moxyCreditUnionEntities)
        {
            if (moxyCreditUnionEntities == null)
                return false;

            DapperPlusManager.Entity<MoxyCreditUnionEntity>().Table(_bulkInsertTableName);

            using (
                IDbConnection db = new SqlConnection(_connectionString))
            {
                db.Execute(_truncateStoredProcedureName, commandType: CommandType.StoredProcedure);
               
                db.BulkInsert(moxyCreditUnionEntities);
                return true;
            }

        }
    }
}
