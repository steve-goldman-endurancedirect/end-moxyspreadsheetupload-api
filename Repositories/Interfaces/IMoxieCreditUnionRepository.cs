﻿using MoxyCreditUnion.Entities;
using System.Collections.Generic;


namespace MoxyCreditUnion.Repositories.Interfaces
{
    public interface IMoxieCreditUnionRepository
    {
        bool SaveMoxieCreditUnionData(IEnumerable<MoxyCreditUnionEntity> MoxyCreditUnionEntities);
    }
}
