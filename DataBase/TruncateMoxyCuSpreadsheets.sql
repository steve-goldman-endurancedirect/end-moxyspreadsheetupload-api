﻿USE [MoxyLive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TruncateMoxyCuSpreadsheetsApi]
AS
BEGIN

	SET NOCOUNT ON;

	truncate table MoxyCuSpreadsheetsApiTest

END
GO

