﻿
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MoxyCuSpreadsheetsApiTest](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[AccountBase] [varchar](10) NOT NULL,
	[AccountType] [varchar](10) NOT NULL,
	[Vin] [varchar](25) NOT NULL,
	[FirstName] [varchar](30) NOT NULL,
	[LastName] [varchar](30) NOT NULL,
	[Address] [varchar](50) NOT NULL,
	[City] [varchar](20) NOT NULL,
	[State] [varchar](10) NOT NULL,
	[Zip] [varchar](10) NOT NULL,
	[AreaCode] [varchar](5) NOT NULL,
	[Phone] [varchar](10) NOT NULL,
	[CellPhone] [varchar](10) NOT NULL,
	[Email] [nvarchar](255) NOT NULL,
	[OriginalAmount] [money] NOT NULL,
	[AmountRemaining] [money] NOT NULL,
	[Rate] [decimal](10, 2) NOT NULL,
	[OriginalTerm] [int] NOT NULL,
	[PaymentsRemaining] [int] NOT NULL,
	[CurrentPayment] [money] NOT NULL,
	[Accountno] [varchar](50) NOT NULL,
	[MoxyCuId] [int] NOT NULL,
	[HasCosigner] [bit] NOT NULL,
	[CoSignerFirstName] [varchar](30) NOT NULL,
	[CoSignerLastName] [varchar](30) NOT NULL,
	[CoSignerPhone] [varchar](10) NOT NULL,
	[CoSignerEmail] [nvarchar](255) NOT NULL
 CONSTRAINT [PK_MoxyCuSpreadsheetsApiTest] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

