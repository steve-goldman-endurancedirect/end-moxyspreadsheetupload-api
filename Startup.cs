﻿using System;
using Autofac.Extensions.DependencyInjection;
using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MoxyCreditUnion.DataAccess;
using MoxyCreditUnion.Repositories.Implementations;
using MoxyCreditUnion.Repositories.Interfaces;
using MoxyCreditUnion.Services.Implementations;
using MoxyCreditUnion.Services.Interfaces;
using MoxyCreditUnion.Helpers;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Swashbuckle.AspNetCore.Swagger;

namespace MoxyCreditUnion
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(env.ContentRootPath)
               .AddJsonFile("appsettings.json", true, true)
               .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
               .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IContainer ApplicationContainer { get; private set; }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            ConfigureAuthService(services);
            services.AddMvc();
            services.Configure<ConnectionConfig>(Configuration.GetSection("ConnectionStrings"));
            services.Configure<ConstantsConfig>(Configuration.GetSection("ApplicationConstants")); 
         

            // Create the container builder.
            var builder = new ContainerBuilder();

            // Note that "CorsPolicy" has to be applied down in the Configure method
            services.AddCors(opt => opt.AddPolicy("CorsPolicy", build => build.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod().AllowCredentials()));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Upload CreditUnion Spreadsheets API", Version = "v1" });
            });

            builder.RegisterType<MoxyCreditUnionService>().As<IMoxyCreditUnionService>();
            builder.RegisterType<MoxieCreditUnionRepository>().As<IMoxieCreditUnionRepository>();
            builder.Populate(services);
            ApplicationContainer = builder.Build();

            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(ApplicationContainer);
        }

        /// <summary>
        /// B. Olges 7/25/2018 - copied in from CreditUnionAPI
        /// </summary>
        /// <param name="services"></param>
        private void ConfigureAuthService(IServiceCollection services)
        {
            // prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            // var appConstants = (ConstantsConfig)(Configuration.GetSection("ApplicationConstants"));

            var identityUrl = Configuration["IdentityUrl"];

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateActor = false,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    RequireExpirationTime = true,
                    RequireSignedTokens = true,
                    ValidIssuer = Configuration["ApplicationConstants:JwtTokenValidIssuer"], // "Endurance",  
                    ValidAudience = Configuration["ApplicationConstants:JwtTokenValidAudience"],// "CRM", 
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes
                                                       (Configuration["ApplicationConstants:JwtTokenIssuerSigningKey"]))  
                };

                //// B. Olges added 7/19/2018 to help with testing
                ////  For production this should be commented out.
                ////  When trying to test with a short JWT timeout, uncomment this
                ////  or else ClockSkew defaults to 5 minutes, which I think makes 5 minutes 
                ////  the shortest timeout you'd see
                //if (!string.IsNullOrEmpty(Configuration["DisableJwtClockSkewForTesting"]))
                //{
                //    if (Configuration["DisableJwtClockSkewForTesting"].ToLower() == "true")
                //    {
                //        options.TokenValidationParameters.ClockSkew = System.TimeSpan.Zero;
                //    }
                //}

                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.Audience = Configuration["ApplicationConstants:JwtTokenValidAudience"]; // "CRM" 

            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireMoxy", policy =>
                {
                    policy.RequireClaim("MoxyCUId"); 
                });
            });
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("CorsPolicy");
            app.UseAuthentication(); 
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Upload CreditUnion Spreadsheets API");
                c.RoutePrefix = string.Empty;
            });
        }
    }
}
