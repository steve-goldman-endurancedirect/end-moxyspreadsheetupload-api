﻿
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using MoxyCreditUnion.Models;
using MoxyCreditUnion.Services.Interfaces;
using Newtonsoft.Json;
using MoxyCreditUnion.Filters;
using System.Net.Http;
using System;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using MoxyCreditUnion.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

namespace MoxyCreditUnion.Controllers
{
    [Route("moxycu")]
    public class MoxieCreditUnionController : Controller
    {
        private readonly IMoxyCreditUnionService _moxyCreditUnionService;
        private readonly string _apiAccessUrl;
        public  MoxieCreditUnionController(IMoxyCreditUnionService moxyCreditUnionService, IOptions<ConstantsConfig> constantsConfig)
        {
            _moxyCreditUnionService = moxyCreditUnionService;
            _apiAccessUrl = constantsConfig.Value.ApiAccessUrl;
        }

        /// <summary>
        /// API Endpoint:  {siteurl}/moxycu/senddata
        /// </summary>
        /// <param name="moxyCreditUnionData"></param>
        [HttpPost("senddata")]
        public IActionResult Post([FromBody] IEnumerable<MoxyCreditUnionModel> moxyCreditUnionData)
        {
            if (Authorize() == HttpStatusCode.OK)
            {
                var isSuccessSave = _moxyCreditUnionService.SaveMoxieCreditUnionData(moxyCreditUnionData);

                if (isSuccessSave)
                    return new OkResult();
            }

            return new UnauthorizedResult();
        }

        /// <summary>
        /// TODO:  Need to setup Authorize as a filter
        /// </summary>
        /// <returns></returns>
        private HttpStatusCode Authorize()
        {
            string currentAuthorizationHeader = HttpContext.Request.Headers["Authorization"];
            if (currentAuthorizationHeader != null && currentAuthorizationHeader.StartsWith("Basic"))
            {
                // Pass into micro-service to validate access
                var webRequest = (HttpWebRequest)WebRequest.Create(_apiAccessUrl);
                webRequest.Headers["Authorization"] = currentAuthorizationHeader;
                webRequest.Method = "POST";
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse != null)
                {
                    return webResponse.StatusCode;
                }
                else
                {
                    return HttpStatusCode.BadRequest;
                }
            }
            return HttpStatusCode.BadRequest;
        }

        [HttpGet("JwtTokenTest"),Authorize(Policy = "RequireMoxy")]
        public IActionResult JwtTokenTest()
        {
            return Ok(new { result = "You got thru to JwtTokenTest" });
        }

        ///// <summary>
        ///// Url {siteurl}/moxycu/senddata/test
        ///// Endpoint used to test this API with a Post Request
        ///// </summary>
        //[HttpGet("senddata/test")]
        //public void Test()
        //{
        //    var accessCode = GetTestAccessCode();
        //    PostToUrl(accessCode);
        //}

        //private static string GetTestAccessCode()
        //{
        //    //Localhttp://localhost:56929/
        //    HttpClient client = new HttpClient()
        //    {

        //        BaseAddress = new Uri("http://apiaccess-dev.azurewebsites.net/apiaccess/getaccesscode")
        //    };

        //    // Add an Accept header for JSON format.
        //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //    var user = new User { UserEmail = "steve.goldman@endurancedirect.com", Password = "pass" };
        //    HttpResponseMessage response = client.PostAsync("GetAccessCode", new StringContent(JsonConvert.SerializeObject(user, Formatting.None), Encoding.UTF8, "application/json")).Result;
        //    HttpContent content = response.Content;
        //    string responseBodyAsText = content.ReadAsStringAsync().Result;
        //    return responseBodyAsText.Replace("\"", string.Empty);
        //}

        //private static void PostToUrl(string accessCode)
        //{
        //    // Local
        //    var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:50596/moxycu/senddata");

        //    // Production
        //    //var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://moxycreditunionapi.azurewebsites.net/moxycu/senddata");

        //    // Development
        //    //var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://moxycreditunionapi-development.azurewebsites.net/moxycu/senddata");


        //    httpWebRequest.ContentType = "application/json";
        //    httpWebRequest.Method = "POST";
        //    httpWebRequest.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(accessCode));

        //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

        //    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //    {
        //        string moxyCreditUnionData = GetJsonData();
        //        streamWriter.Write(moxyCreditUnionData);
        //        streamWriter.Flush();
        //        streamWriter.Close();
        //    }

        //    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //    httpResponse.Close();

        //}

        //private static string GetJsonData()
        //{
        //    var MoxyCreditUnionItems = new List<MoxyCreditUnionModel>();

        //    var item1 = new MoxyCreditUnionModel
        //    {
        //        AccountBase = "4407",
        //        AccountType = "22",
        //        Vin = "KM8J33A27GU060370",
        //        FirstName = "RICHARD",
        //        LastName = "GILBERTSON",
        //        Address = "PO BOX 436",
        //        City = "MIDLOTHIAN",
        //        State = "IL",
        //        Zip = "60445",
        //        AreaCode = "",
        //        Phone = "8154649111",
        //        CellPhone = "7085330501",
        //        Email = "RICHG411SBCDSL@YAHOO.COM",
        //        OriginalAmount = (decimal)25900.00,
        //        AmountRemaining = (decimal)13922.71,
        //        Rate = (decimal)1.89,
        //        OriginalTerm = 48,
        //        PaymentsRemaining = 19,
        //        CurrentPayment = (decimal)452.98,
        //        Accountno = "SD4407-22",
        //        MoxyCuId = 5
        //    };
        //    var item2 = new MoxyCreditUnionModel
        //    {
        //        AccountBase = "4409",
        //        AccountType = "23",
        //        Vin = "KM8J33A27GU060354",
        //        FirstName = "TJ",
        //        LastName = "Watts",
        //        Address = "PO BOX 520",
        //        City = "Chicago",
        //        State = "IL",
        //        Zip = "60613",
        //        AreaCode = "",
        //        Phone = "8154649111",
        //        CellPhone = "7085330501",
        //        Email = "test11SBCDSL@YAHOO.COM",
        //        OriginalAmount = (decimal)25900.00,
        //        AmountRemaining = (decimal)13422.71,
        //        Rate = (decimal)1.89,
        //        OriginalTerm = 48,
        //        PaymentsRemaining = 19,
        //        CurrentPayment = (decimal)452.98,
        //        Accountno = "SD8887-22",
        //        MoxyCuId = 5
        //    };
        //    MoxyCreditUnionItems.Add(item1);
        //    MoxyCreditUnionItems.Add(item2);

        //    for (int i = 1; i <= 1000; i++)
        //    {
        //        MoxyCreditUnionItems.Add(new MoxyCreditUnionModel
        //        {
        //            AccountBase = "4409",
        //            AccountType = "23",
        //            Vin = "KM8J33A27GU060354",
        //            FirstName = "TJ",
        //            LastName = "Watts",
        //            Address = "PO BOX 520" + i,
        //            City = "Chicago",
        //            State = "IL",
        //            Zip = "60613",
        //            AreaCode = "",
        //            Phone = "8154649111",
        //            CellPhone = "7085330501",
        //            Email = "test11SBCDSL@YAHOO.COM",
        //            OriginalAmount = (decimal)25900.00,
        //            AmountRemaining = (decimal)13422.71,
        //            Rate = (decimal)1.89,
        //            OriginalTerm = 48,
        //            PaymentsRemaining = 19,
        //            CurrentPayment = (decimal)452.98,
        //            Accountno = "SD8887-22",
        //            MoxyCuId = 5
        //        });

        //    }

        //    return JsonConvert.SerializeObject(MoxyCreditUnionItems, Formatting.None);
        //}
    }

    //public class User
    //{
    //    public string UserEmail { get; set; }
    //    public string Password { get; set; }
    //}
}
