﻿using MoxyCreditUnion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoxyCreditUnion.Services.Interfaces
{
    public interface IMoxyCreditUnionService
    {
        bool SaveMoxieCreditUnionData(IEnumerable<MoxyCreditUnionModel> moxieCreditUnionDataModels);
    }
}
