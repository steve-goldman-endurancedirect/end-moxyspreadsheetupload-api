﻿using MoxyCreditUnion.Services.Interfaces;
using MoxyCreditUnion.Models;
using System.Collections.Generic;
using System.Linq;
using MoxyCreditUnion.Entities;
using MoxyCreditUnion.Repositories.Interfaces;
using Endurance.ExtensionMethods;

namespace MoxyCreditUnion.Services.Implementations
{
    public class MoxyCreditUnionService : IMoxyCreditUnionService
    {
        private readonly IMoxieCreditUnionRepository _moxieCreditUnionRepository;

        public MoxyCreditUnionService(IMoxieCreditUnionRepository moxieCreditUnionRepository)
        {
            _moxieCreditUnionRepository = moxieCreditUnionRepository;
        }

        public bool SaveMoxieCreditUnionData(IEnumerable<MoxyCreditUnionModel> moxieCreditUnionDataModels)
        {
            return _moxieCreditUnionRepository.SaveMoxieCreditUnionData(ConvertToEntities(moxieCreditUnionDataModels)) ;
        }

        private MoxyCreditUnionEntity GetMoxyCreditUnionEntity(MoxyCreditUnionModel moxyCreditUnionModel)
        {
            return new MoxyCreditUnionEntity
            {
                AccountBase = string.IsNullOrEmpty(moxyCreditUnionModel.AccountBase) ? string.Empty : moxyCreditUnionModel.AccountBase.TrimToLength(10),
                AccountType = string.IsNullOrEmpty(moxyCreditUnionModel.AccountType) ? string.Empty : moxyCreditUnionModel.AccountType.TrimToLength(10),
                Vin = string.IsNullOrEmpty(moxyCreditUnionModel.Vin) ? string.Empty : moxyCreditUnionModel.Vin.TrimToLength(25),
                FirstName = string.IsNullOrEmpty(moxyCreditUnionModel.FirstName) ? string.Empty : moxyCreditUnionModel.FirstName.TrimToLength(30),
                LastName = string.IsNullOrEmpty(moxyCreditUnionModel.LastName) ? string.Empty : moxyCreditUnionModel.LastName.TrimToLength(30),
                Address = string.IsNullOrEmpty(moxyCreditUnionModel.Address) ? string.Empty : moxyCreditUnionModel.Address.TrimToLength(50),
                City = string.IsNullOrEmpty(moxyCreditUnionModel.City) ? string.Empty : moxyCreditUnionModel.City.TrimToLength(20),
                State = string.IsNullOrEmpty(moxyCreditUnionModel.State) ? string.Empty : moxyCreditUnionModel.State.TrimToLength(10),
                Zip = string.IsNullOrEmpty(moxyCreditUnionModel.Zip) ? string.Empty : moxyCreditUnionModel.Zip.TrimToLength(10),
                AreaCode = string.IsNullOrEmpty(moxyCreditUnionModel.AreaCode) ? string.Empty : moxyCreditUnionModel.AreaCode.TrimToLength(5),
                Phone = string.IsNullOrEmpty(moxyCreditUnionModel.Phone) ? string.Empty : moxyCreditUnionModel.Phone.TrimToLength(10),
                CellPhone = string.IsNullOrEmpty(moxyCreditUnionModel.CellPhone) ? string.Empty : moxyCreditUnionModel.CellPhone.TrimToLength(10),
                Email = string.IsNullOrEmpty(moxyCreditUnionModel.Email) ? string.Empty : moxyCreditUnionModel.Email.TrimToLength(255),
                OriginalAmount = moxyCreditUnionModel.OriginalAmount ?? 0,
                AmountRemaining = moxyCreditUnionModel.AmountRemaining ?? 0,
                Rate = moxyCreditUnionModel.Rate ?? 0,
                OriginalTerm = moxyCreditUnionModel.OriginalTerm ?? 0,
                PaymentsRemaining = moxyCreditUnionModel.PaymentsRemaining ?? 0,
                CurrentPayment = moxyCreditUnionModel.CurrentPayment ?? 0,
                Accountno = string.IsNullOrEmpty(moxyCreditUnionModel.Accountno) ? string.Empty : moxyCreditUnionModel.Accountno.TrimToLength(50),
                MoxyCuId = moxyCreditUnionModel.MoxyCuId ?? 0,
                HasCosigner = moxyCreditUnionModel.HasCosigner ?? false,
                CoSignerFirstName = string.IsNullOrEmpty(moxyCreditUnionModel.CoSignerFirstName) ? string.Empty : moxyCreditUnionModel.CoSignerFirstName.TrimToLength(30),
                CoSignerLastName = string.IsNullOrEmpty(moxyCreditUnionModel.CoSignerLastName) ? string.Empty : moxyCreditUnionModel.CoSignerLastName.TrimToLength(30),
                CoSignerEmail = string.IsNullOrEmpty(moxyCreditUnionModel.CoSignerEmail) ? string.Empty : moxyCreditUnionModel.CoSignerEmail.TrimToLength(10),
                CoSignerPhone = string.IsNullOrEmpty(moxyCreditUnionModel.CoSignerPhone) ? string.Empty : moxyCreditUnionModel.CoSignerPhone.TrimToLength(255)
            };
        }

        private IEnumerable<MoxyCreditUnionEntity> ConvertToEntities(IEnumerable<MoxyCreditUnionModel> moxieCreditUnionDataModels)
        {
            var moxyCreditUnionEntities = new List<MoxyCreditUnionEntity>();
            if (moxieCreditUnionDataModels != null)
            { 
                moxieCreditUnionDataModels.ToList().ForEach(x => moxyCreditUnionEntities.Add(GetMoxyCreditUnionEntity(x)));
            }
            return moxyCreditUnionEntities;
        }
    }
}
