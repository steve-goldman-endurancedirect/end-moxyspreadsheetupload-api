﻿
namespace MoxyCreditUnion.Helpers
{
    public class ConstantsConfig
    {
        public string MoxyCuTableName { get; set; }
        public string MoxyCuTruncateStoredProcedureName { get; set; }
        public string ApiAccessUrl { get; set; }
    }
}
