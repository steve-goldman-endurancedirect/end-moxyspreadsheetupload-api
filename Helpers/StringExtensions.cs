﻿using System;

namespace Endurance.ExtensionMethods
{
    public static partial class StringExtensions
    {
        public static string TrimToLength(this String str, int length)
        {
            return str.Trim().Substring(0, str.Length<length?str.Length:length);
        }
    }
}
