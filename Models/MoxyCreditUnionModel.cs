﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoxyCreditUnion.Models
{
    public class MoxyCreditUnionModel
    {
        public string AccountBase { get; set; }
        public string AccountType { get; set; }
        public string Vin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string AreaCode { get; set; }
        public string Phone { get; set; }
        public string CellPhone { get; set; }
        public string Email { get; set; }
        public decimal? OriginalAmount { get; set; }
        public decimal? AmountRemaining { get; set; }
        public decimal? Rate { get; set; }
        public int? OriginalTerm { get; set; }
        public int? PaymentsRemaining { get; set; }
        public decimal? CurrentPayment { get; set; }
        public string Accountno { get; set; }
        public int? MoxyCuId { get; set; }
        public bool? HasCosigner { get; set; }
        public string CoSignerFirstName { get; set; }
        public string CoSignerLastName { get; set; }
        public string CoSignerPhone { get; set; }
        public string CoSignerEmail { get; set; }
    }
}
