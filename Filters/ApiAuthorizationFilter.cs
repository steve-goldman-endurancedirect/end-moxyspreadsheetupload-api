﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MoxyCreditUnion.Filters
{


    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class ApiAuthorizationAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        public ApiAuthorizationAttribute()
        {
          
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string currentAuthorizationHeader = context.HttpContext.Request.Headers["Authorization"];
            if (currentAuthorizationHeader != null && currentAuthorizationHeader.StartsWith("Basic"))
            {
                // Pass into micro-service to validate access
                var webRequest = (HttpWebRequest)WebRequest.Create("http://localhost:56929/apiaccess/isvalidapirequest");
                webRequest.Headers["Authorization"] = currentAuthorizationHeader;
                webRequest.Method = "POST";
                var webResponse = (HttpWebResponse)webRequest.GetResponse();
                if (webResponse != null)
                {
                    if (webResponse.StatusCode == HttpStatusCode.OK)
                    {
                        return;
                    }
                    else if (webResponse.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        context.Result = new UnauthorizedResult();
                    }
                    else
                    {
                        context.Result = new BadRequestResult();
                    }
                }
                else
                {
                    context.Result = new BadRequestResult();
                }
            }
        }
    }

    //public class ApiAuthorizationFilter : AuthorizeAttribute, IAuthorizationFilter
    //{
    //    private readonly string realm;

    //    public ApiAuthorizationFilter(string realm=null) 
    //    {
    //        this.realm = realm;
    //    }

    //    public void OnAuthorization(AuthorizationFilterContext context)
    //    {
    //        string currentAuthorizationHeader = context.HttpContext.Request.Headers["Authorization"];
    //        if (currentAuthorizationHeader != null && currentAuthorizationHeader.StartsWith("Basic"))
    //        {
    //            // Pass into micro-service to validate access
    //            var webRequest = (HttpWebRequest)WebRequest.Create("http://localhost:56929/apiaccess/isvalidapirequest");
    //            webRequest.Headers["Authorization"] = currentAuthorizationHeader;
    //            webRequest.Method = "POST";
    //            var webResponse = (HttpWebResponse)webRequest.GetResponse();
    //            if (webResponse != null)
    //            {
    //                if (webResponse.StatusCode == HttpStatusCode.OK)
    //                {
    //                    return;
    //                }
    //                else if (webResponse.StatusCode == HttpStatusCode.Unauthorized)
    //                {
    //                    context.Result = new UnauthorizedResult();
    //                }
    //                else
    //                {
    //                    context.Result = new BadRequestResult();
    //                }
    //            }
    //            else
    //            {
    //                context.Result = new BadRequestResult();
    //            }
    //        }
    //    }
    //}
}
